<?php

/**
 * @file
 * Uw_strategic_plan_2020.features.filter.inc .
 */

/**
 * Implements hook_filter_default_formats().
 */
function uw_strategic_plan_2020_filter_default_formats() {
  $formats = array();
  // Exported format: Marketing item.
  $formats['uw_tf_sph_marketing_item'] = array(
    'format' => 'uw_tf_sph_marketing_item',
    'name' => 'Marketing item',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'image_resize_filter' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(
          'link' => 0,
          'link_class' => '',
          'link_rel' => '',
          'image_locations' => array(
            'local' => 'local',
            'remote' => 0,
          ),
        ),
      ),
      'uw_filters_strip_tags' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(),
      ),
      'ckeditor_link_filter' => array(
        'weight' => -42,
        'status' => 1,
        'settings' => array(),
      ),
      'wysiwyg' => array(
        'weight' => -37,
        'status' => 1,
        'settings' => array(
          'valid_elements' => '@[class|title|id|lang|dir<ltr?rtl],a[href|rel|rev|name],abbr/acronym, sub, sup, dfn, samp, kbd, var,ol[start],button,em, i, strong, b, strike, s, del, ins, cite, blockquote, address, img, code, pre, ul, li[value], dl, dt, dd, p, h2, h3, h4, h5, h6, span, div[data|data-theme], thead, tfoot, tbody, tr, caption, hr, br',
          'allow_comments' => 0,
          'nofollow_policy' => 'disabled',
          'nofollow_domains' => array(),
          'style_color' => array(
            'color' => 0,
            'background' => 0,
            'background-color' => 0,
            'background-image' => 0,
            'background-repeat' => 0,
            'background-attachment' => 0,
            'background-position' => 0,
          ),
          'style_font' => array(
            'font' => 0,
            'font-family' => 0,
            'font-size' => 0,
            'font-size-adjust' => 0,
            'font-stretch' => 0,
            'font-style' => 0,
            'font-variant' => 0,
            'font-weight' => 0,
          ),
          'style_text' => array(
            'text-align' => 0,
            'text-decoration' => 0,
            'text-transform' => 0,
            'letter-spacing' => 0,
            'word-spacing' => 0,
            'white-space' => 0,
            'direction' => 0,
            'unicode-bidi' => 0,
          ),
          'style_box' => array(
            'margin' => 0,
            'margin-top' => 0,
            'margin-right' => 0,
            'margin-bottom' => 0,
            'margin-left' => 0,
            'padding' => 0,
            'padding-top' => 0,
            'padding-right' => 0,
            'padding-bottom' => 0,
            'padding-left' => 0,
          ),
          'style_border-1' => array(
            'border' => 0,
            'border-top' => 0,
            'border-right' => 0,
            'border-bottom' => 0,
            'border-left' => 0,
            'border-width' => 0,
            'border-top-width' => 0,
            'border-right-width' => 0,
            'border-bottom-width' => 0,
            'border-left-width' => 0,
          ),
          'style_border-2' => array(
            'border-color' => 0,
            'border-top-color' => 0,
            'border-right-color' => 0,
            'border-bottom-color' => 0,
            'border-left-color' => 0,
            'border-style' => 0,
            'border-top-style' => 0,
            'border-right-style' => 0,
            'border-bottom-style' => 0,
            'border-left-style' => 0,
          ),
          'style_dimension' => array(
            'height' => 0,
            'line-height' => 0,
            'max-height' => 0,
            'max-width' => 0,
            'min-height' => 0,
            'min-width' => 0,
            'width' => 0,
          ),
          'style_positioning' => array(
            'bottom' => 0,
            'clip' => 0,
            'left' => 0,
            'overflow' => 0,
            'right' => 0,
            'top' => 0,
            'vertical-align' => 0,
            'z-index' => 0,
          ),
          'style_layout' => array(
            'clear' => 0,
            'display' => 0,
            'float' => 0,
            'position' => 0,
            'visibility' => 0,
          ),
          'style_list' => array(
            'list-style' => 0,
            'list-style-image' => 0,
            'list-style-position' => 0,
            'list-style-type' => 0,
          ),
          'style_table' => array(
            'border-collapse' => 0,
            'border-spacing' => 0,
            'caption-side' => 0,
            'empty-cells' => 0,
            'table-layout' => 0,
          ),
          'style_user' => array(
            'cursor' => 0,
            'outline' => 0,
            'outline-width' => 0,
            'outline-style' => 0,
            'outline-color' => 0,
            'zoom' => 0,
          ),
          'rule_valid_classes' => array(
            0 => 'a*',
            2 => 'b*',
            4 => 'c*',
            6 => 'd*',
            8 => 'e*',
            10 => 'f*',
            12 => 'g*',
            14 => 'h*',
            16 => 'i*',
            18 => 'j*',
            20 => 'k*',
            22 => 'l*',
            24 => 'm*',
            26 => 'n*',
            28 => 'o*',
            30 => 'p*',
            32 => 'q*',
            34 => 'r*',
            36 => 's*',
            38 => 't*',
            40 => 'u*',
            42 => 'v*',
            44 => 'w*',
            46 => 'x*',
            48 => 'y*',
            50 => 'z*',
            52 => 'A*',
            54 => 'B*',
            56 => 'C*',
            58 => 'D*',
            60 => 'E*',
            62 => 'F*',
            64 => 'G*',
            66 => 'H*',
            68 => 'I*',
            70 => 'J*',
            72 => 'K*',
            74 => 'L*',
            76 => 'M*',
            78 => 'N*',
            80 => 'O*',
            82 => 'P*',
            84 => 'Q*',
            86 => 'R*',
            88 => 'S*',
            90 => 'T*',
            92 => 'U*',
            94 => 'V*',
            96 => 'W*',
            98 => 'X*',
            100 => 'Y*',
            102 => 'Z*',
          ),
          'rule_bypass_valid_classes' => 0,
          'rule_valid_ids' => array(
            0 => 'a*',
            2 => 'b*',
            4 => 'c*',
            6 => 'd*',
            8 => 'e*',
            10 => 'f*',
            12 => 'g*',
            14 => 'h*',
            16 => 'i*',
            18 => 'j*',
            20 => 'k*',
            22 => 'l*',
            24 => 'm*',
            26 => 'n*',
            28 => 'o*',
            30 => 'p*',
            32 => 'q*',
            34 => 'r*',
            36 => 's*',
            38 => 't*',
            40 => 'u*',
            42 => 'v*',
            44 => 'w*',
            46 => 'x*',
            48 => 'y*',
            50 => 'z*',
            52 => 'A*',
            54 => 'B*',
            56 => 'C*',
            58 => 'D*',
            60 => 'E*',
            62 => 'F*',
            64 => 'G*',
            66 => 'H*',
            68 => 'I*',
            70 => 'J*',
            72 => 'K*',
            74 => 'L*',
            76 => 'M*',
            78 => 'N*',
            80 => 'O*',
            82 => 'P*',
            84 => 'Q*',
            86 => 'R*',
            88 => 'S*',
            90 => 'T*',
            92 => 'U*',
            94 => 'V*',
            96 => 'W*',
            98 => 'X*',
            100 => 'Y*',
            102 => 'Z*',
          ),
          'rule_bypass_valid_ids' => 0,
          'rule_style_urls' => array(),
          'rule_bypass_style_urls' => 0,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -36,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  // Exported format: Standard.
  $formats['uw_tf_standard'] = array(
    'format' => 'uw_tf_standard',
    'name' => 'Standard',
    'cache' => 1,
    'status' => 1,
    'weight' => -10,
    'filters' => array(
      'image_resize_filter' => array(
        'weight' => -50,
        'status' => 1,
        'settings' => array(
          'link' => 0,
          'link_class' => '',
          'link_rel' => '',
          'image_locations' => array(
            'local' => 'local',
            'remote' => 0,
          ),
        ),
      ),
      'uw_filters_strip_tags' => array(
        'weight' => -49,
        'status' => 1,
        'settings' => array(),
      ),
      'ckeditor_link_filter' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(),
      ),
      'wysiwyg' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(
          'valid_elements' => '@[class|title|id|lang|dir<ltr?rtl],a[href|rel|rev|name|target<_self|download],img[!src|alt|longdesc|width|height],uwvideo[!href],table[width],th[abbr|axis|headers|scope|colspan|rowspan|width],td[abbr|axis|headers|scope|colspan|rowspan|width],colgroup[span|width],col[span|width],div[data-reveal-id|tabindex|role],abbr/acronym, sub, sup, dfn, samp, kbd, var,ol[start],em,figure,figcaption, i, strong, b, strike, s, del, ins, footer, cite, blockquote, address, code, pre, ul, li[value], dl, dt, dd, p, h2, h3, h4, h5, h6, span, div, thead, tfoot, tbody, tr, caption, hr, br,ckcalltoaction[data-calltoaction-nid],cktwitter[data-type|data-username|data-listname|data-displayname|data-tweet|data-url|data-timeline],ckfacebook[data-type|data-username|data-displayname|data-fburl|data-fullpost],ckfactsfigures[data-factsfigures-nid|data-usecarousel|data-numberpercarousel],ckimagegallery[data-imagegallerynid|data-gallerytype],cklivestream[data-username|data-displayname],ckmailchimp[data-sourcecode],ckmailman[data-listname|data-servername],cktableau[data-url|data-height|data-tabs|data-server|data-site],cktdx[data-tdx-id|data-tdx-show-project-title],cktint[data-id|data-keywords|data-height|data-columns],cktimeline[data-restfulurl|data-timeline-nid],ckvimeo[data-url],ckembeddedmaps[data-type|data-src|data-height],ckhootsuite[data-subdomain|data-height]',
          'allow_comments' => 1,
          'nofollow_policy' => 'disabled',
          'nofollow_domains' => array(),
          'style_color' => array(
            'color' => 0,
            'background' => 0,
            'background-color' => 0,
            'background-image' => 0,
            'background-repeat' => 0,
            'background-attachment' => 0,
            'background-position' => 0,
          ),
          'style_font' => array(
            'font' => 0,
            'font-family' => 0,
            'font-size' => 0,
            'font-size-adjust' => 0,
            'font-stretch' => 0,
            'font-style' => 0,
            'font-variant' => 0,
            'font-weight' => 0,
          ),
          'style_text' => array(
            'text-align' => 0,
            'text-decoration' => 0,
            'text-indent' => 0,
            'text-transform' => 0,
            'letter-spacing' => 0,
            'word-spacing' => 0,
            'white-space' => 0,
            'direction' => 0,
            'unicode-bidi' => 0,
          ),
          'style_box' => array(
            'margin' => 0,
            'margin-top' => 0,
            'margin-right' => 0,
            'margin-bottom' => 0,
            'margin-left' => 0,
            'padding' => 0,
            'padding-top' => 0,
            'padding-right' => 0,
            'padding-bottom' => 0,
            'padding-left' => 0,
          ),
          'style_border-1' => array(
            'border' => 0,
            'border-top' => 0,
            'border-right' => 0,
            'border-bottom' => 0,
            'border-left' => 0,
            'border-width' => 0,
            'border-top-width' => 0,
            'border-right-width' => 0,
            'border-bottom-width' => 0,
            'border-left-width' => 0,
          ),
          'style_border-2' => array(
            'border-color' => 0,
            'border-top-color' => 0,
            'border-right-color' => 0,
            'border-bottom-color' => 0,
            'border-left-color' => 0,
            'border-style' => 0,
            'border-top-style' => 0,
            'border-right-style' => 0,
            'border-bottom-style' => 0,
            'border-left-style' => 0,
            'border-radius' => 0,
          ),
          'style_dimension' => array(
            'height' => 0,
            'line-height' => 0,
            'max-height' => 0,
            'max-width' => 0,
            'min-height' => 0,
            'min-width' => 0,
            'width' => 0,
          ),
          'style_positioning' => array(
            'bottom' => 0,
            'clip' => 0,
            'left' => 0,
            'overflow' => 0,
            'right' => 0,
            'top' => 0,
            'vertical-align' => 0,
            'z-index' => 0,
          ),
          'style_layout' => array(
            'clear' => 0,
            'display' => 0,
            'float' => 0,
            'position' => 0,
            'visibility' => 0,
          ),
          'style_list' => array(
            'list-style' => 0,
            'list-style-image' => 0,
            'list-style-position' => 0,
            'list-style-type' => 0,
          ),
          'style_table' => array(
            'border-collapse' => 0,
            'border-spacing' => 0,
            'caption-side' => 0,
            'empty-cells' => 0,
            'table-layout' => 0,
          ),
          'style_user' => array(
            'cursor' => 0,
            'outline' => 0,
            'outline-width' => 0,
            'outline-style' => 0,
            'outline-color' => 0,
            'zoom' => 0,
          ),
          'rule_valid_classes' => array(
            0 => 'a*',
            2 => 'b*',
            4 => 'c*',
            6 => 'd*',
            8 => 'e*',
            10 => 'f*',
            12 => 'g*',
            14 => 'h*',
            16 => 'i*',
            18 => 'j*',
            20 => 'k*',
            22 => 'l*',
            24 => 'm*',
            26 => 'n*',
            28 => 'o*',
            30 => 'p*',
            32 => 'q*',
            34 => 'r*',
            36 => 's*',
            38 => 't*',
            40 => 'u*',
            42 => 'v*',
            44 => 'w*',
            46 => 'x*',
            48 => 'y*',
            50 => 'z*',
            52 => 'A*',
            54 => 'B*',
            56 => 'C*',
            58 => 'D*',
            60 => 'E*',
            62 => 'F*',
            64 => 'G*',
            66 => 'H*',
            68 => 'I*',
            70 => 'J*',
            72 => 'K*',
            74 => 'L*',
            76 => 'M*',
            78 => 'N*',
            80 => 'O*',
            82 => 'P*',
            84 => 'Q*',
            86 => 'R*',
            88 => 'S*',
            90 => 'T*',
            92 => 'U*',
            94 => 'V*',
            96 => 'W*',
            98 => 'X*',
            100 => 'Y*',
            102 => 'Z*',
          ),
          'rule_bypass_valid_classes' => 0,
          'rule_valid_ids' => array(
            0 => 'a*',
            2 => 'b*',
            4 => 'c*',
            6 => 'd*',
            8 => 'e*',
            10 => 'f*',
            12 => 'g*',
            14 => 'h*',
            16 => 'i*',
            18 => 'j*',
            20 => 'k*',
            22 => 'l*',
            24 => 'm*',
            26 => 'n*',
            28 => 'o*',
            30 => 'p*',
            32 => 'q*',
            34 => 'r*',
            36 => 's*',
            38 => 't*',
            40 => 'u*',
            42 => 'v*',
            44 => 'w*',
            46 => 'x*',
            48 => 'y*',
            50 => 'z*',
            52 => 'A*',
            54 => 'B*',
            56 => 'C*',
            58 => 'D*',
            60 => 'E*',
            62 => 'F*',
            64 => 'G*',
            66 => 'H*',
            68 => 'I*',
            70 => 'J*',
            72 => 'K*',
            74 => 'L*',
            76 => 'M*',
            78 => 'N*',
            80 => 'O*',
            82 => 'P*',
            84 => 'Q*',
            86 => 'R*',
            88 => 'S*',
            90 => 'T*',
            92 => 'U*',
            94 => 'V*',
            96 => 'W*',
            98 => 'X*',
            100 => 'Y*',
            102 => 'Z*',
          ),
          'rule_bypass_valid_ids' => 0,
          'rule_style_urls' => array(),
          'rule_bypass_style_urls' => 0,
        ),
      ),
      'uw_video_embed' => array(
        'weight' => -46,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -45,
        'status' => 1,
        'settings' => array(),
      ),
      'ckeditor_socialmedia' => array(
        'weight' => -44,
        'status' => 1,
        'settings' => array(),
      ),
      'tablesaw' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'ckeditor_embedded_cta' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'ckeditor_embedded_ff' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'ckeditor_embedded' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'uw_ct_image_gallery' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'uw_embedded_maps' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'ckeditor_embedded_tdx' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'uw_org_chart' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );
  return $formats;
}
