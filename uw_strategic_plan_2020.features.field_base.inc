<?php

/**
 * @file
 * uw_strategic_plan_2020.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_strategic_plan_2020_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_uw_sph_hero_type'.
  $field_bases['field_uw_sph_hero_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_uw_sph_hero_type',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'imagination' => 'imagination',
        'research' => 'research',
        'education' => 'education',
        'community' => 'community',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
