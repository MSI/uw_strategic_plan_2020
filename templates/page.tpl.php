<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['global_header']: Items for the header region.
 * - $page['content']: The main content of the current page.
 * - $page['mid_content']: Items for insert in the middle of the page content
 *   (handled in page templates).
 * - $page['global_footer']: Items for the footer region.
 * - $page['login_link']: Items for the login link area.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
$global_message = file_get_contents('https://uwaterloo.ca/global-message.html');

?>
<div class="breakpoint"></div>
<div id="site" data-nav-visible="false" class="<?php if ($global_message) echo 'global-message--active'; ?> <?php print $classes; ?> uw-site"<?php print $attributes; ?>>
  <div class="uw-site--inner">
    <div class="uw-section--inner">
        <div id="skip" class="skip">
            <a href="#main" class="element-invisible element-focusable uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
            <a href="#footer" class="element-invisible element-focusable  uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
        </div>
    </div>
    <div id="header" class="uw-header--global">
        <?php

        if ($global_message) {
            ?>
            <div id="global-message">
                <?php echo $global_message; ?>
            </div>
            <?php
        }
        ?>

        <div class="uw-section--inner">
      <?php print render($page['global_header']); ?>
      </div>
    </div>
    <div id="site--offcanvas" class="uw-site--off-canvas">
      <div class="uw-section--inner">
      <?php print render($page['site_header']); ?>
      </div>
    </div>
    <?php if (variable_get('uw_theme_branding', 'full') == "full") { ?>
    <div id="site-colors" class="uw-site--colors">
        <div class="uw-section--inner">
            <div class="uw-site--cbar">
                <div class="uw-site--c1 uw-cbar"></div>
                <div class="uw-site--c2 uw-cbar"></div>
                <div class="uw-site--c3 uw-cbar"></div>
                <div class="uw-site--c4 uw-cbar"></div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="uw-site--main-top">
      <div class="uw-site--banner">
        <?php print render($page['banner']); ?>
      </div>
      <div class="uw-site--inner">
        <div class="uw-site--messages">
        <?php print $messages; ?>
        </div>
        <div class="uw-site--help">
            <?php print render($page['help']); ?>
        </div>
        <!-- When logged in -->
        <?php if ($tabs): ?>
        <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?>
        </div>
        <?php endif; ?>
        <?php if ($action_links): ?>
        <ul class="action-links">
        <?php print render($action_links); ?>
        </ul>
        <?php endif; ?>
      </div>
    </div>
    <div id="main" class="uw-site--main" role="main">
    <?php print render($page['content']); ?>
    <?php print $feed_icons; ?>
    </div>
    <div id="footer" class="uw-footer" role="contentinfo">
      <div id="uw-site-share" class="uw-site-share">
        <div class="uw-section--inner">
          <div class="uw-section-share">
          </div>
          <ul class="uw-site-share-top">
            <li class="uw-site-share--button__top">
              <a href="#main" id="uw-top-button" class="uw-top-button">
              <i class="ifdsu fdsu-arrow"></i>
              <span class="uw-footer-top-word">TOP</span></a>
            </li>
            <li class="uw-site-share--button__share">
              <a href="#uw-site-share" class="uw-footer-social-button">
              <i class="ifdsu fdsu-share"></i>
              <span class="uw-footer-share-word">Share</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div id="site-footer" class="uw-site-footer site-footer-toggle open-site-footer">
        <div class="uw-section--inner">
          <div class="uw-site-footer1<?php print empty($page['site_footer']) ? ' uw-no-site-footer' : ''; ?>">
            <div class="uw-site-footer1--logo-dept">
              <?php
              $site_logo = variable_get('uw_nav_site_footer_logo');
              $site_logo_link = variable_get('uw_nav_site_footer_logo_link');
              $facebook = variable_get('uw_nav_site_footer_facebook');
              $twitter = variable_get('uw_nav_site_footer_twitter');
              $instagram = variable_get('uw_nav_site_footer_instagram');
              $youtube = variable_get('uw_nav_site_footer_youtube');
              $linkedin = variable_get('uw_nav_site_footer_linkedin');
              $snapchat = variable_get('uw_nav_site_footer_snapchat');

              $alt_tag = str_replace('_', ' ', variable_get('uw_nav_site_footer_logo')) . ' logo';
              $arr = explode(" ", $alt_tag);
              $arr = array_unique($arr);
              $alt_tag = implode(" ", $arr);
              ?>
              <?php if (isset($site_logo_link)) : ?>
                <a href="<?php print $site_logo_link; ?>"><img src="<?php print base_path() . drupal_get_path('module', 'uw_nav_site_footer'); ?>/logos/<?php print variable_get('uw_nav_site_footer_logo'); ?>.png" alt="<?php print $alt_tag; ?>" /></a>
              <?php else : ?>
                <?php $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>'; ?>
              <?php endif; ?>
            </div>
            <div class="uw-site-footer1--contact">
              <ul class="uw-footer-social">
                <?php if ($facebook !== "" && $facebook !== NULL): ?>
                  <li>
                    <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>">
                      <i class="ifdsu fdsu-facebook"></i>
                      <span class="off-screen">facebook</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($twitter !== "" && $twitter !== NULL): ?>
                  <li>
                    <a href="https://www.twitter.com/<?php print check_plain($twitter); ?>">
                      <i class="ifdsu fdsu-twitter"></i>
                      <span class="off-screen">twitter</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($youtube !== "" && $youtube !== NULL): ?>
                  <li>
                    <a href="https://www.youtube.com/<?php print check_plain($youtube); ?>">
                      <i class="ifdsu fdsu-youtube"></i>
                      <span class="off-screen">youtube</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($instagram !== "" && $instagram !== NULL): ?>
                  <li>
                    <a href="https://www.instagram.com/<?php print check_plain($instagram); ?>">
                      <i class="ifdsu fdsu-instagram"></i>
                      <span class="off-screen">instagram</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($linkedin !== "" && $linkedin !== NULL): ?>
                  <li>
                    <a href="https://www.linkedin.com/<?php check_plain(print $linkedin); ?>">
                      <i class="ifdsu fdsu-linkedin"></i>
                      <span class="off-screen">linkedin</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($snapchat !== "" && $snapchat !== NULL): ?>
                  <li>
                    <a href="https://www.snapchat.com/add/<?php check_plain(print $snapchat); ?>">
                      <i class="ifdsu fdsu-snapchat"></i>
                      <span class="off-screen">snapchat</span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <?php print render($page['global_footer']); ?>
    </div>
  </div>
</div><!--/site-->
