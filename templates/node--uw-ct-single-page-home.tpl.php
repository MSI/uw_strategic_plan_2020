<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<?php
global $base_path;
$theme_path = $base_path . drupal_get_path('theme', 'uw_theme_marketing');
?>
<?php if (isset($field_uw_sph_banner_image[0])) :?>
<?php
    $banner = array();
    $banner_image = file_create_url($field_uw_sph_banner_image[0]['uri']);
    $banner_alt = $field_uw_sph_banner_image[0]['alt'];
    $banner_small[0]['img_location_small'] = image_style_url('banner_small', $field_uw_sph_banner_image[0]['uri']);
    $banner_med[0]['img_location_med'] = image_style_url('banner_med', $field_uw_sph_banner_image[0]['uri']);
    $banner_large[0]['img_location_large'] = image_style_url('banner_large', $field_uw_sph_banner_image[0]['uri']);
    $banner_xl[0]['img_location_xl'] = image_style_url('banner_xl', $field_uw_sph_banner_image[0]['uri']);
?>
<script>
 ;(function($, window, document) {
     'use strict';
     $(document).ready(checkWidth);
      function checkWidth () {
      var $banner = $('#banner-image');
      var $window = $(window);
      var window_width = document.documentElement.clientWidth || document.body.clientWidth;
      if (window_width <= 641) {
        $banner.attr("src", "<?php echo $banner_small['0']['img_location_small']; ?>") ;
      }
      else if (window_width <= 768) {
        $banner.attr("src", "<?php echo $banner_med['0']['img_location_med']; ?>");
      }
      else if (window_width <= 1023) {
        $banner.attr("src", "<?php echo $banner_large['0']['img_location_large']; ?>");
      }
      else if (window_width  >=1024){
        $banner.attr("src", "<?php echo $banner_xl['0']['img_location_xl']; ?>");
      }
     }
     checkWidth();
     $(window).resize(checkWidth);


 })(window.jQuery, window, window.document);

</script>

<?php endif; ?>
<?php if (isset($node->field_banner_type['und']) && $node->field_banner_type['und'][0]['value'] == 'video') :?>
    <div class="videoWrapper">
        <video playsinline autoplay muted loop aria-label="<?php print $node->field_banner_video['und'][0]['description']; ?>">
            <source src="<?php print strip_tags(render($content['field_banner_video'])); ?>" type="video/mp4">
        </video>
        <div class="video-bottom">
            <div class="content-body">
                <div class="controls">
                    <button class="video-control pause" aria-label="Play"></button>
                </div>
            </div>
        </div>
    </div>
    <?php if (isset($node->field_banner_description['und']) && $node->field_banner_type['und'][0]['value'] == 'video') : ?>
        <div class="video-description">
            <div class="content-body">
                <?php print $node->field_banner_description['und'][0]['value']; ?>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<?php if (isset($field_uw_sph_banner_image[0])) :?>
    <div id="banner-area" class="uw-site--banner">
        <div class="uw-section--inner">
            <div class="banner-item">
                <?php if (!empty($field_uw_sph_banner_image[0]['image_field_caption']['value'])) : ?>
                    <div class="banner-text">
                        <div class="highlight"><?php print $field_uw_sph_banner_image[0]['image_field_caption']['value']; ?></div>
                    </div>
                <?php endif; ?>
            </div>

            <!-- Addition html to customize the strategic plan presentation.-->
            <?php if (isset($node->field_uw_sph_hero_type['und'][0]['value'])) : ?>
            <div class="artworks-container" data-theme="<?php print $node->field_uw_sph_hero_type['und'][0]['value']; ?>">
            <?php else: ?>
            <div class="artworks-container" data-theme="imagination">
            <?php endif; ?>
                <div class="artworks-title-wrap">

                   <div class="artworks-title page-title-wrap"> <h1><?php print $title ? $title : $site_name; ?></h1></div>
                   <div class="uw-site--subtitle">
                       <span>University of Waterloo </span><span>STRATEGIC PLAN / 2020 - 2025</span>
                   </div>
               </div>

                <div class="uw-strat-caption">
                    <button class="canvas-toggle">Turn Animation<span class="canvas-on-off"> Off</span> </button>
                    <p>
                        <a href="#artwork">
                            <span class="strat-caption-title">About the interactive digital art:</span>
                        </a>
                        <span class="canvas-details-js">&ndash; This conceptual art uses a subset of data from more than 80,000 degrees earned by Waterloo students between 2008 and 2018. Click or tap and hold to generate different art.</span>
                        <span class="canvas-details">&ndash; Your browser does not support the interactive features of this artwork. Please use either Chrome or Firefox web browsers and ensure you have JavaScript enabled.</span>

                    </p>
                </div>
            </div>
            <!--End of Addition html to customize the strategic plan presentation.-->

        </div>
    </div>

<?php endif; ?>

  <div class="uw-site--main-content">
    <div id="content" class="uw-site-content node-<?php print $node->nid; ?> <?php print $classes; ?>"<?php print $attributes; ?>>
      <div class="content content-body">
        <div class="uw-section--inner">
             <?php print render($content['body']); ?>
        </div>
      </div>
      <div class="content mid-content">
          <div class="uw-section--inner">
            <?php
            // Render the mid-content region.
            $mid_content = block_get_blocks_by_region('mid_content');
            print render($mid_content);
            ?>
          </div>
      </div>
        <div class="content"<?php print $content_attributes; ?>>
        <?php
        // We hide the comments and links now
        // so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        // Hide content we've already rendered in a non-standard way (doing it
        // the standard way would hide it automatically).
        hide($content['field_uw_sph_banner_image']);
        hide($content['field_banner_video']);
        print render($content);
        ?>
        </div>
        <?php
        // Remove the "Add new comment" link on the
        // teaser page or if the comment form
        // is being displayed on the same page.
        if ($teaser || !empty($content['comments']['comment_form'])) : ?>
            <?php
            unset($content['links']['comment']['#links']['comment-add']);

            // Only display the wrapper div if there are links.
            $links = render($content['links']);
            ?>

            <?php if($links):?>
                <div class="link-wrapper">
                    <?php print $links; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    <?php print render($content['comments']); ?>
    </div>
  </div><!--/site main-->
