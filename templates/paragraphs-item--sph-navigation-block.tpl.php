<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
hide($content['field_sph_display_title']);
hide($content['field_sph_block_title']);
global $base_path;
?>
<div id="sph-navigation-wrapper" tabindex="0">
  <div class="sph-navigation-inner" >
    <?php print render($content); ?>
    <!-- add the link to the homepage to the bottom of the menu -->
    <div class="paragraphs-items paragraphs-items-field-sph-navigation-items paragraphs-items-field-sph-navigation-items-full paragraphs-items-full">
      <a href="https://uwaterloo.ca/">
        <div class="sph-navigation-item">
          <div class="sph-navigation-text">
            University of Waterloo - Home
          </div>
          <div class="sph-navigation-icon">
            <img src="<?php echo $base_path . drupal_get_path('module', 'uw_strategic_plan_2020'); ?>/images/crest3.png" alt="university of waterloo crest">
          </div>
        </div>
      </a>
    </div>
  </div>
</div>
