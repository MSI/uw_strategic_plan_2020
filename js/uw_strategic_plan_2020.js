/**
 * @file
 */
(function ($) {
  "use strict";
  Drupal.behaviors.fdsuTheme = {
    attach: function (context, settings) {
      // Prvent scrolling on Body when menu is open.
      $('button.rmc-nav__navigation-button').on('click', function () {
        $('html').toggleClass('no-scroll');
      });
    }
  };
}(jQuery));

(function ($) {

      // Check if an element is visible in the window.
      function check_for_element(obj) {
        var $t            = obj,
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = obj === true ? _bottom : _top,
            compareBottom = obj === true ? _top : _bottom;
        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
      }

      // On scroll check if the heading content is visible.
      // Add and remove animate class.
      $(window).scroll(function (event) {
        $('.sph-heading-block--facts-wrapper').each(function () {
          if (check_for_element($(this))) {
            $(this).addClass("animate");
          }
          else {
            $(this).removeClass("animate");
          }
        });
      });


      // On the load of the page or when adding a paragraph, ensure that only the URL appears.
      $.each($(".field-name-field-event-url input"), function (i, url) {
        // If the URL is null or blank, hide it.
        if ($(url).val() == "" || $(url).val() == null) {
          $container = $(url).closest('td');
          $title = $container.find("div[id*='field-event-title']");
          var fieldset = $container.find('fieldset');

          $title.hide();
          $(fieldset[1]).hide();
        }
      });

      $.each($(".field-name-field-generic-event-url input"), function (i, url) {
        // If the URL is null or blank, hide it.
        if ($(url).val() == "" || $(url).val() == null) {
          $container = $(url).closest('td');
          $title = $container.find("div[id*='field-generic-event-title']");
          var fieldset = $container.find('fieldset');

          $title.hide();
          $(fieldset[1]).hide();
        }
      });

      // Add a listener to each input or a STEM Remote Event URL.
      $('div[class*="field-event-url"] input').live('input', function () {
        var url = $(this).val();
        var matches = url.match(/http(.*)\:\/\/(.*)uwaterloo\.ca\/(.*)\/events\/(.*)/);
        $input = $(this);

        // Once a match of https://uwaterloo.ca/<site_url>/event/, load in the data.
        if (matches.length > 0) {

          // Make call to API based on the event_id.
          $.getJSON("https://api.uwaterloo.ca/v2/events/search.json?key=dem0hash&link=" + encodeURIComponent(url), function (event_details) {

            $container = $input.closest('td');
            $title = $container.find("div[id*='field-event-title']");
            var fieldset = $container.find('fieldset');

            // If there is an id, show the event and fill in the details.
            if (event_details.data.length > 0) {

              // If there is a message remove it, since we have found a remote event by this point.
              if ($input.parent().find('label').find('span.messages').length > 0) {
                $input.parent().find('label > span.messages').remove();
              }

              // Step through each date/time and add it to the appropriate places on the page.
              $.each(event_details.data[0].times, function (j, event_time_date) {
                // Check if there is a start date.
                if (event_time_date.start.length > 0) {
                  var start_time_hours, start_time_mins, start_time_suffix;
                  var end_time_hours, end_time_mins, end_time_suffix;
                  var start_time_present = false;
                  var end_time_present = false;

                  // Check if there is a start time.
                  if (event_time_date.start_time.length > 0) {
                    start_time_present = true;

                    // Get hours and minutes.
                    start_time_hours = event_time_date.start_time.substring(0,2);
                    start_time_mins = event_time_date.start_time.substring(3,5);

                    // Convert 24 hour to 12 hour.
                    start_time_suffix = (start_time_hours >= 12) ? 'pm' : 'am';
                    start_time_hours = (start_time_hours > 12) ? start_time_hours - 12 : start_time_hours;
                    start_time_hours = (start_time_hours == '00') ? 12 : start_time_hours;
                  }

                  // Check if there is an end time.
                  if (event_time_date.end_time.length > 0) {
                    end_time_present = true;

                    // Get hours and minutes.
                    end_time_hours = event_time_date.end_time.substring(0,2);
                    end_time_mins = event_time_date.end_time.substring(3,5);

                    // Convert 24 hour to 12 hour.
                    end_time_suffix = (end_time_hours >= 12) ? 'pm' : 'am';
                    end_time_hours = (end_time_hours > 12) ? end_time_hours - 12 : end_time_hours;
                    end_time_hours = (end_time_hours == '00') ? 12 : end_time_hours;
                  }

                  // Insert start time into the page.
                  $(fieldset[1]).find("input[id*='value-date']").val(event_time_date.start_date);
                  $(fieldset[1]).find("input[id*='value-time']").val(start_time_hours + ":" + start_time_mins + " " + start_time_suffix);

                  // Insert end time into the page.
                  $(fieldset[1]).find("input[id*='value2-date']").val(event_time_date.end_date);
                  $(fieldset[1]).find("input[id*='value2-time']").val(end_time_hours + ":" + end_time_mins + " " + end_time_suffix);
                }

                // Insert title into the page.
                $title.find('input').val(event_details.data[0].title);

                // Insert the description into the event teaser on the page.  Must use the CKEDITOR instances to access the editor.
                $event_teaser = $container.find("div[id*='field-event-teaser-und-0-value']");
                var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
                CKEDITOR.instances[event_teaser_id].setData(event_details.data[0].description_raw);

                // Insert the cost into the page. If it is Free ensure that a 0 zero value is added.
                if ($.isNumeric(event_details.data[0].cost)) {
                  $container.find("input[id*='field-sph-event-cost']").val(event_details.data[0].cost);
                }
                else if (event_details.data[0].cost !== null && event_details.data[0].cost.toLowerCase() == "free") {
                  $container.find("input[id*='field-sph-event-cost']").val('0');
                }
              });

              // Show title and event details.
              $title.show();
              $(fieldset[1]).show();
            }
            else {
              if ($input.parent().find('label').find('span.messages').length == 0) {
                $input.parent().find('label').append('<span class="messages error"><span class="element-invisible">Error:</span>Unable to retrieve remote event.  Please ensure you have the correct URL.</span>');
              }
              // Set the start time to blank.
              $(fieldset[1]).find("input[id*='value-date']").val('');
              $(fieldset[1]).find("input[id*='value-time']").val('');

              // Set the end time to blank.
              $(fieldset[1]).find("input[id*='value2-date']").val('');
              $(fieldset[1]).find("input[id*='value2-time']").val('');

              // Set the title to blank.
              $title.find('input').val('');

              // Set the cost to blank.
              $container.find("input[id*='field-sph-event-cost']").val('');

              // Set the event teaser to blank.
              $event_teaser = $container.find("div[id*='field-generic-event-teaser-und-0-value']");
              var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
              CKEDITOR.instances[event_teaser_id].setData('');

              // Hide the title and the fieldset.
              $title.hide();
              $(fieldset[1]).hide();
            }
          })
              .error(function () {
                if ($input.parent().find('label').find('span.messages').length == 0) {
                  $input.parent().find('label').append('<span class="messages error"><span class="element-invisible">Error:</span>Unable to look up remote events at this time.</span>');
                }
              });
        }
        else {
          alert('NON WCMS');
        }
      });

      // Add a listener to each input or a Remote Event URL.
      $('div[class*="field-generic-event-url"] input').live('input', function () {
        var url = $(this).val();
        var matches = url.match(/http(.*)\:\/\/(.*)uwaterloo\.ca\/(.*)\/events\/(.*)/);
        $input = $(this);

        // Once a match of https://uwaterloo.ca/<site_url>/event/, load in the data.
        if (matches.length > 0) {

          // Make call to API based on the event_id.
          $.getJSON("https://api.uwaterloo.ca/v2/events/search.json?key=dem0hash&link=" + encodeURIComponent(url), function (event_details) {

            $container = $input.closest('td');
            $title = $container.find("div[id*='field-generic-event-title']");
            var fieldset = $container.find('fieldset');

            // If there is an id, show the event and fill in the details.
            if (event_details.data.length > 0) {

              // If there is a message remove it, since we have found a remote event by this point.
              if ($input.parent().find('label').find('span.messages').length > 0) {
                $input.parent().find('label > span.messages').remove();
              }

              // Convert dates of each event into UNIX epoch time and set them in an array.
              var timestamps = [];
              var current_time = event_details.meta.timestamp * 1000;
              $.each(event_details.data[0].times,function (j, event_start) {
                timestamps[j] = (new Date(event_start.start)).getTime();
              });

              // Find the index of the next upcoming event in the array.
              var closest_event_index = timestamps.length - 1;
              $.each(timestamps, function (i, event_time_stamp) {
                if (current_time < event_time_stamp) {
                  closest_event_index = i;
                  return false;
                }
              });

              // Add upcoming event to the page.
              var event_time_date = event_details.data[0].times[closest_event_index];
              (function () {
                // Check if there is a start date.
                if (event_time_date.start.length > 0) {
                  var start_time_hours, start_time_mins, start_time_suffix;
                  var end_time_hours, end_time_mins, end_time_suffix;
                  var start_time_present = false;
                  var end_time_present = false;

                  // Check if there is a start time.
                  if (event_time_date.start_time.length > 0) {
                    start_time_present = true;

                    // Get hours and minutes.
                    start_time_hours = event_time_date.start_time.substring(0,2);
                    start_time_mins = event_time_date.start_time.substring(3,5);

                    // Convert 24 hour to 12 hour.
                    start_time_suffix = (start_time_hours >= 12) ? 'pm' : 'am';
                    start_time_hours = (start_time_hours > 12) ? start_time_hours - 12 : start_time_hours;
                    start_time_hours = (start_time_hours == '00') ? 12 : start_time_hours;
                  }

                  // Check if there is an end time.
                  if (event_time_date.end_time.length > 0) {
                    end_time_present = true;

                    // Get hours and minutes.
                    end_time_hours = event_time_date.end_time.substring(0,2);
                    end_time_mins = event_time_date.end_time.substring(3,5);

                    // Convert 24 hour to 12 hour.
                    end_time_suffix = (end_time_hours >= 12) ? 'pm' : 'am';
                    end_time_hours = (end_time_hours > 12) ? end_time_hours - 12 : end_time_hours;
                    end_time_hours = (end_time_hours == '00') ? 12 : end_time_hours;
                  }

                  // Insert start time into the page.
                  $(fieldset[1]).find("input[id*='value-date']").val(event_time_date.start_date);
                  $(fieldset[1]).find("input[id*='value-time']").val(start_time_hours + ":" + start_time_mins + " " + start_time_suffix);

                  // Insert end time into the page.
                  $(fieldset[1]).find("input[id*='value2-date']").val(event_time_date.end_date);
                  $(fieldset[1]).find("input[id*='value2-time']").val(end_time_hours + ":" + end_time_mins + " " + end_time_suffix);
                }

                // Insert title into the page.
                $title.find('input').val(event_details.data[0].title);

                // Insert the description into the event teaser on the page.  Must use the CKEDITOR instances to access the editor.
                $event_teaser = $container.find("div[id*='field-generic-event-teaser-und-0-value']");
                var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
                CKEDITOR.instances[event_teaser_id].setData(event_details.data[0].description_raw);

                // Insert the cost into the page. If it is Free ensure that a 0 zero value is added.
                if ($.isNumeric(event_details.data[0].cost)) {
                  $container.find("input[id*='field-sph-event-cost']").val(event_details.data[0].cost);
                }
                else if (event_details.data[0].cost !== null && event_details.data[0].cost.toLowerCase() == "free") {
                  $container.find("input[id*='field-sph-event-cost']").val('0');
                }
              }());

              // Show title and event details.
              $title.show();
              $(fieldset[1]).show();
            }
            else {
              if ($input.parent().find('label').find('span.messages').length == 0) {
                $input.parent().find('label').append('<span class="messages error"><span class="element-invisible">Error:</span>Unable to retrieve remote event.  Please ensure you have the correct URL.</span>');
              }
              // Set the start time to blank.
              $(fieldset[1]).find("input[id*='value-date']").val('');
              $(fieldset[1]).find("input[id*='value-time']").val('');

              // Set the end time to blank.
              $(fieldset[1]).find("input[id*='value2-date']").val('');
              $(fieldset[1]).find("input[id*='value2-time']").val('');

              // Set the title to blank.
              $title.find('input').val('');

              // Set the cost to blank.
              $container.find("input[id*='field-sph-event-cost']").val('');

              // Set the event teaser to blank.
              $event_teaser = $container.find("div[id*='field-generic-event-teaser-und-0-value']");
              var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
              CKEDITOR.instances[event_teaser_id].setData('');

              // Hide the title and the fieldset.
              $title.hide();
              $(fieldset[1]).hide();
            }
          })
              .error(function () {
                if ($input.parent().find('label').find('span.messages').length == 0) {
                  $input.parent().find('label').append('<span class="messages error"><span class="element-invisible">Error:</span>Unable to look up remote events at this time.</span>');
                }
              });
        }
        else {
          alert('NON WCMS');
        }
      });

      // Clicking play/pause button.
      $('.videoWrapper .controls .video-control').click(function () {
        if ($(".videoWrapper video").get(0).paused) {
          $(".videoWrapper video").get(0).play();
          $(this).removeClass("play");
          $(this).addClass("pause");
          $(this).attr('aria-label', 'Pause');
        }
        else {
          $(".videoWrapper video").get(0).pause();
          $(this).removeClass("pause");
          $(this).addClass("play");
          $(this).attr('aria-label', 'Play');
        }
      });

      // Hiding description.
      $('.videoWrapper .controls .description-control').click(function () {
        $(".videoWrapper .description").slideToggle();
        $(this).toggleClass("down");
        $(this).toggleClass("up");
      });

      function sph_nav() {

    // Get the hash from the URL.
    var hash = window.location.hash.substr(1);

    // Set up variable telling us where the nav is.
    var $navitems = $('#sph-navigation-wrapper');

    // Set up the nav items
    var $navlinks = $('.paragraphs-items-field-sph-navigation-items a');

    // Get the screen width.
    var screenWidth = $(window).width();

    // If there are navigation items and on large screens, process them.
    if ($navitems.length && $navitems.offset() && screenWidth > 769) {

      // Get the height of the navigation.
      var navheight = $('#header').outerHeight();

      // Get the admin bar height.
      var admin_offset = 0;
      if ($('#navbar-bar').length) {
        admin_offset += $('#navbar-bar').height();
      }

      if ($('.navbar-tray.navbar-active').length) {
        admin_offset += $('.navbar-tray.navbar-active').height();
      }

      // If there is a anchor link being used, process it.
      if (hash !== '') {

        // Get current position of window.
        windowpos = $(window).scrollTop();

        $('a[id^="' + hash + '"]').css('padding-top', navheight);

      }

      // Don't do anything until the page finishes loading completely, including images.
      $(window).on('load', function () {


        // If a anchor link is clicked, scroll to it.
        $('a[href^="#"]').on('click', function (event) {

          // Set active class to nav item
          $navlinks.removeClass('active');
          $(this).addClass('active');
          $(this).blur();

          // Reset all the paddings for the anchor links.
          $('.sph-anchor-link-navigation').each(function () {
            $(this).css('padding-top', '0');
          });

          event.preventDefault();

          // Get the anchor_id from the link element.
          var anchor_id = $(this).attr('href');
          anchor_id = anchor_id.substring(1, anchor_id.length);

          // Set the variables for the target, paragraph and offset.
          var target = $($(this).attr('href')),
              p = $(target).offset().top,
              offset = navheight;

          // Set the paragraph.
          $(target).hasClass('sph-anchor-link-navigation') && (p = p - offset);

          // Scroll to the anchor link.
          $('body, html').animate({ scrollTop: p }, 250);

          // Set the anchor link in the URL.
          history.pushState(null, null, '#' + anchor_id);
        });

      });
    }
  }

      (function toggleCanvas($, window, document) {
        'use strict';
        $(document).ready(handleDocumentReady);

        function handleDocumentReady() {
          sph_nav();

          $('#tabpanel-main-menu ul li a').click(function(){
            $('button.navigation-button').click();
          });
          $('.canvas-toggle').click(function(e) {
            e.preventDefault();
            if ($('body').hasClass('canvas-on')){

              $('body').toggleClass("canvas-on").toggleClass("canvas-off");

              $('.canvas-on-off').text('On');
            }
            else{
              $('body').hasClass('canvas-off');
              $('body').toggleClass("canvas-off").toggleClass("canvas-on");

              $('.canvas-on-off').text('Off');
            }
          })
        };
      })(window.jQuery, window, window.document);

      (function hovernav($, window, document) {
        'use strict';
        $(document).ready(handleDocumentReady);

        function handleDocumentReady() {
          $('#sph-navigation-wrapper').hover(function() {
            $(this).addClass('hovered');
          },function(){
            $(this).removeClass('hovered');
          });

          var inFocus = false;

          $('#sph-navigation-wrapper').focus(function() {
            inFocus = true;
            $(this).addClass('hovered');
          });
          $('#sph-navigation-wrapper').blur(function() {
            inFocus = false;
            $(this).removeClass('hovered');
          });

          $('.paragraphs-items-field-sph-navigation-items a').focus(function() {
            inFocus = true;
            $('#sph-navigation-wrapper').addClass('hovered');
          });
          $('.paragraphs-items-field-sph-navigation-items a').blur(function() {
            inFocus = false;
             $('#sph-navigation-wrapper').removeClass('hovered');
          });

        };
      })(window.jQuery, window, window.document);


      $(window).load(function isIe() {
        var ua = window.navigator.userAgent;
        var oldIe = ua.indexOf('MSIE ');
        var newIe = ua.indexOf('Trident/');
        if ((oldIe > -1) || (newIe > -1)) {
          $('html').addClass('isie-old');
          }
      });


  // Add event listener on window resize for video loading.
  window.addEventListener('resize', videoLoad);

  // Add event listener on window resize for the navigation.
  window.addEventListener('resize', sph_nav);

  // Ensuring that video does not load at smaller screen sizes.
  function videoLoad() {

    var screenWidth = $(window).width();

    if (screenWidth < 769) {
      $('video').removeAttr('autoplay');
      $('video').attr('preload', 'none');
    }
    else {
      $('video').attr('autoplay', 'true');
      $('video').removeAttr('preload');
    }
  }
  $(document).ready(function () {

    // Ensuring that video does not load at smaller screen sizes.
    videoLoad();

    // Ensure that the code for the nav loads on the page.
    sph_nav();

    // ****** COPY BLOCK ******.
    $('.field-name-field-sph-copy > .expandable > h2:first-child').click(function () {
      $expandable = $(this).parent();
      if ($expandable.hasClass('expanded')) {
        $expandable.removeClass('expanded');
        $('.expandable-content',$expandable).prev().addClass('last-visible');
      }
      else {
        $expandable.addClass('expanded');
        $('.expandable-content',$expandable).prev().removeClass('last-visible');
      }
    }).wrapInner('<button>');

    // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins
    // .node-type-uw-web-page.
    $('.field-name-field-sph-copy > .expandable > .expandable-content').prev().addClass('last-visible');

    // .node-type-uw-web-page.
    $('.field-name-field-sph-copy > .expandable > .expandable-content :last-child').addClass('last-visible');

    // If there is more than one expandable region on the page, add expand/collapse all functions.
    if ($('.field-name-field-sph-copy > .expandable').length > 1) {

      // .node-type-uw-web-page.
      $('.field-name-field-sph-copy > .expandable:first').before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');

      $('#content .expandable-controls .expand-all').click(function () {
        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable:not(.expanded) h2:first-child').click();
      });

      $('#content .expandable-controls .collapse-all').click(function () {

        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable.expanded h2:first-child').click();
      });
    }

    // ****** BODY SINGLE PAGE ******.
    $('.field-name-body > .expandable > h2:first-child').click(function () {
      $expandable = $(this).parent();

      if ($expandable.hasClass('expanded')) {
        $expandable.removeClass('expanded');
        $('.expandable-content',$expandable).prev().addClass('last-visible');
      }
      else {
        $expandable.addClass('expanded');
        $('.expandable-content',$expandable).prev().removeClass('last-visible');
      }
    }).wrapInner('<button>');

    // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins
    // .node-type-uw-web-page.
    $('.field-name-body > .expandable > .expandable-content').prev().addClass('last-visible');

    // .node-type-uw-web-page.
    $('.field-name-body > .expandable > .expandable-content :last-child').addClass('last-visible');

    // If there is more than one expandable region on the page, add expand/collapse all functions.
    if ($('.field-name-body > .expandable').length > 1) {
      // .node-type-uw-web-page.
      $('.field-name-body > .expandable:first').before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');

      $('#content .expandable-controls .expand-all').click(function () {
        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable:not(.expanded) h2:first-child').click();
      });

      $('#content .expandable-controls .collapse-all').click(function () {
        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable.expanded h2:first-child').click();
      });
    }

    // Do things when the anchor changes.
    window.onhashchange = uw_fdsu_anchors;

    // Trigger the event on page load in case there is already an anchor.
    uw_fdsu_anchors();

    // Function to do things when the anchor changes.
    function uw_fdsu_anchors() {
      if (location.hash) {
        // Check if there is an ID with this name.
        if ($(location.hash).length) {
          // If it's in an unexpanded expandable content area, expand the expandable content area.
          $(location.hash,'.expandable:not(.expanded)').closest('.expandable').find('h2:first-child').click();
          // Scroll to the ID, taking into account the toolbar if it exists
          // "html" works in Firefox, "body" works in Chrome/Safari.
          $('html, body').scrollTop($(location.hash).offset().top - $('#toolbar').height());
        }
      }
    }
  });
})(jQuery);
(function labelClick($, window, document) {
  'use strict';
  function setFocusToTextBox(id){
    document.getElementById(id).focus();
  }
  function handleDocumentReady() {
    $("input[id^='CheckboxSwitch']").each(function () {
      $('label[for="' + $(this).attr('id') + '"]').on('keydown', function (e) {
        var code = e.which;
        if ((code === 13) || (code === 32)) {
          $(this).click();
          setFocusToTextBox($(this).attr('id'));
        }
      });
    });
  }
  $(document).ready(handleDocumentReady);


})(window.jQuery, window, window.document);
