<?php

/**
 * @file
 * Code for the Single page home feature.
 */

include_once 'uw_strategic_plan_2020.features.inc';

/**
 * Implements hook_preprocess_html().
 */
function uw_strategic_plan_2020_preprocess_html(&$variables) {
  // Add uw-site--strat-plan class to body tag array.
  $variables['classes_array'][] = "uw-site--strat-plan";
  $variables['classes'] = implode(' ', $variables['classes_array']);

  // Add uw_strategic_plan_2020.js.
  $module_path = drupal_get_path('module', 'uw_strategic_plan_2020') . '/js/';
  drupal_add_js(array('uw_strategic_plan_2020' => array('jsonPath' => $module_path)), array('type' => 'setting'));
}

/**
 * Implements hook_preprocess_page().
 */
function uw_strategic_plan_2020_preprocess_page(&$vars) {
  if (drupal_is_front_page()) {
    // Set then send the banner image to be a background image
    // of the banner with  drupal add css inline.
    $banner_image = file_create_url($vars['page']['content']['system_main']['nodes'][3]['field_uw_sph_banner_image']['#items'][0]['uri']);
    drupal_add_css('  #banner-area:before{background-size:cover;background-image:url(' . $banner_image . ');}',
      array(
        'group' => CSS_THEME,
        'type' => 'inline',
        'media' => 'screen',
      ));
  }
}

/**
 * Implements hook_page_alter().
 *
 * On page load, Grab the module 's
 * index.js file and load it in the footer.
 */
function uw_strategic_plan_2020_page_alter(&$page) {

  $module_path = drupal_get_path('module', 'uw_strategic_plan_2020');
  drupal_add_js($module_path . '/js/index.js',
    array(
      'type' => 'file',
      'scope' => 'footer',
    ));
}

/**
 * Implements hook_js_alter().
 *
 * Removing the .js file from the single page module and using my altered
 * version.
 */
function uw_strategic_plan_2020_js_alter(&$javascript) {
  $path = drupal_get_path('module', 'uw_ct_single_page_home');
  $paththeme = drupal_get_path('theme', 'uw_theme_marketing');
  $exclude = array(
    $path . '/js/uw_ct_single_page_home.js' => TRUE,
    $paththeme . '/scripts/utm.js' => TRUE,
  );
  $javascript = array_diff_key($javascript, $exclude);
}

/**
 * Implements theme_registry_alter().
 *
 * Grab the .tpl files in my module treat them as themes.
 */
function uw_strategic_plan_2020_theme_registry_alter(&$theme_registry) {
  // Path to the module.
  $module_path = drupal_get_path('module', 'uw_strategic_plan_2020') . '/templates';
  // Find all .tpl.php files in this module's folder recursively.
  $template_file_objects = drupal_find_theme_templates($theme_registry, '.tpl.php', $module_path);
  // Iterate through all found template file objects.
  foreach ($template_file_objects as $key => $template_file_object) {
    // If the template has not already been overridden by a theme.
    // Alter the theme path and template elements.
    $theme_registry[$key]['theme paths'] = array($module_path);
    $theme_registry[$key]['theme path'] = $module_path;
    $theme_registry[$key] = array_merge($theme_registry[$key], $template_file_object);
    $theme_registry[$key]['type'] = 'module';
  }
}
