<?php

/**
 * @file
 * uw_strategic_plan_2020.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_strategic_plan_2020_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-uw_ct_single_page_home-field_uw_sph_hero_type'.
  $field_instances['node-uw_ct_single_page_home-field_uw_sph_hero_type'] = array(
    'bundle' => 'uw_ct_single_page_home',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'list',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 10,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_uw_sph_hero_type',
    'label' => 'Banner hero type',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_select',
      'weight' => 16,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Banner hero type');

  return $field_instances;
}
